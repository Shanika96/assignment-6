#include<stdio.h>
int fibonacciSeq(int);
void main ()
{
    int n,i;
    printf("Up to which Fibonacci Number series do you want to print? ");
    scanf("%d",&n);
    for (i = 0; i <= n; i++)
        {
            printf("%d\n", fibonacciSeq(i));
        }
}
int fibonacciSeq(int i)
{
    if (i==0)
    {
    return 0;
    }
    else if (i==1)
    {
    return 1;
    }
    else
    {
    return fibonacciSeq(i-1)+fibonacciSeq(i-2);
    }
}
